export const checkValidity = (value, rules) => {
	let isValid = { valid: true };
	
	if (!rules) {
		return true;
	}

	if (rules.required && isValid.valid) {
		isValid = value.trim() !== '' && isValid.valid ? { valid: true } : { valid: false, error: "Pole nie może być puste" };
	}

	if (rules.minLength && isValid.valid) {
		isValid = value.length >= rules.minLength && isValid.valid ? { valid: true } : { valid: false, error: `Wartość nie może być krótsza niż ${rules.minLength}` };
	}

	if (rules.maxLength && isValid.valid) {
		isValid = value.length <= rules.maxLength && isValid.valid ? { valid: true } : { valid: false, error: `Wartość nie może być dłuższa niż ${rules.maxLength}` };
	}

	if (rules.isEmail && isValid.valid) {
		const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
		isValid = pattern.test(value) && isValid.valid ? { valid: true } : { valid: false, error: `Wprowadzona wartość musi być adresem E-mail` };
	}

	return isValid;
}