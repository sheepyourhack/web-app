import React from 'react';
import { MDBInput, MDBIcon } from 'mdbreact';

import styles from './Input.module.scss';

const input = (props) => {
	let inputElement = [];

	switch (props.elementType) {
		case 'input':
			inputElement.push(<MDBInput
				value={props.value}
				label={props.label}
				icon={props.icon}
				group
				hidden={props.hidden ? true : false}
				disabled={props.disabled ? true : false}
				type={props.inputType}
				onChange={props.onChange}
				onFocus={props.onChange}
				onBlur={props.onBlur}
				key={props.label}
			/>)
			break;
		case 'iconSelect':
			inputElement.push(

				<div key={props.label + 'div'} onClick={props.onClick} className={styles.selectContainer} >
					<MDBIcon
						icon={props.icon}
						key={props.label}
					/>
					<div className={styles.text}>{props.label}</div>
				</div>
			)
			break;
		case 'outlineInput':
			inputElement.push(<MDBInput
				value={props.value}
				label={props.label}
				icon={props.icon}
				group
				outline
				hidden={props.hidden ? true : false}
				disabled={props.disabled ? true : false}
				type={props.inputType}
				onChange={props.onChange}
				onFocus={props.onFocus}
				onBlur={props.onBlur}
				key={props.label + 'INPUT'}
			/>)
			break;
		case 'select':
			var options = props.inputType.split(",");
			for (let i = 0; i < options.length; i++) {
				inputElement.push(<option
					value={options[i].toLowerCase()}
					key={options[i]}
				>{options[i]}</option>)
			}
			break;
		case 'textarea':
			inputElement.push(<MDBInput
				value={props.value}
				label={props.label}
				icon={props.icon}
				group
				hidden={props.hidden ? true : false}
				disabled={props.disabled ? true : false}
				type={props.inputType}
				onChange={props.onChange}
				onFocus={props.onChange}
			/>)
			break;
		default:
			inputElement.push(<MDBInput
				value={props.value}
				label={props.label}
				icon={props.icon}
				group
				hidden={props.hidden ? true : false}
				disabled={props.disabled ? true : false}
				type={props.inputType}
				onChange={props.onChange}
				onFocus={props.onChange}
				onBlur={props.onBlur}
			/>)
	}
	return (
		<React.Fragment>
			{props.elementType !== 'select' ? <div>{inputElement}</div> : <div><MDBIcon icon={props.icon} style={{paddingRight: '15px'}}/>{props.label}: <select onChange={props.changeSelect}>{inputElement}</select></div>}

			{!props.validation.valid && props.showError ? <p className={styles.error}>{props.validation.error}</p> : null}
		</React.Fragment>
	);
}

export default input;