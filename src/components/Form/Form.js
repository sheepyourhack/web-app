import React, { Component } from 'react';
import Input from './Input/Input';
import { MDBBtn } from 'mdbreact';

class Form extends Component {
	render() {
		// make array with form inputs from object
		const formElementsArray = [];
		for (const key in this.props.fields) {
			formElementsArray.push({
				key: key,
				...this.props.fields[key]
			});
		}

		// add Input components rendered from formElementsArray
		const form = formElementsArray.map(formElement => (
			<Input
				key={formElement.key}
				elementType={formElement.elementType}
				inputType={formElement.inputType}
				value={formElement.value}
				label={formElement.label}
				icon={formElement.icon}
				validation={formElement.validation}
				hidden={formElement.hidden}
				disabled={formElement.disabled}
				showError={this.props.showError}
				onChange={(e) => this.props.inputChangedHandler(e, formElement.key)}
				onFocus={this.props.addNewInput ? (e) => this.props.addNewInput(e, formElement.key) : () => console.log()}
				changeSelect={this.props.changeSelect ? (e) => this.props.changeSelect(e, formElement.key) : () => console.log()}
			/>
		));

		return (
			<form style={{ padding: 20 }}>
				{this.props.type === 'select' ?
					< div style={{
						display: 'flex',
						flexDirection: 'row',
						flexWrap: 'wrap',
						justifyContent: 'center',
						alignContent: 'center',
					}}>{form}</div>
					:
					<div>
						{form}
					</div>
				}
				<div className="text-center">
					{
						(this.props.button === undefined || this.props.button) ?
							<MDBBtn className="font-weight-bold" size="sm" color="unique" onClick={this.props.clickHandler}>{this.props.buttonText}</MDBBtn>
							:
							null
					}
				</div>
			</form>
		);
	}
}

export default Form;