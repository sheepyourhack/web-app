import * as actionTypes from './actionTypes';

export const authCompany = (token, company) => {
	return {
		type: actionTypes.AUTH_COMPANY,
		token: token,
		company: company
	}
}

export const updateCompanyData = (company) => {
	return {
		type: actionTypes.UPDATE_COMPANY_DATA,
		company
	}
}

export const logoutCompany = () => {
	return {
		type: actionTypes.LOGOUT_COMPANY
	}
}