import * as actionTypes from './actionTypes';

export const authUser = (token, user) => {
	return {
		type: actionTypes.AUTH_USER,
		token: token,
		user: user
	}
}

export const logoutUser = () => {
	return {
		type: actionTypes.LOGOUT_USER
	}
}