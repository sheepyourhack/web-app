import * as actionTypes from '../actions/actionTypes';

const initialState = {
	user: null,
	token: null
}

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.AUTH_USER: {
			return {
				...state,
				token: action.token,
				user: action.user
			}
		}
		case actionTypes.LOGOUT_USER: {
			return {
				...state,
				user: null,
				token: null
			}
		}
		default:
			return state;
	}
}

export default reducer;