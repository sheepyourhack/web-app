import * as actionTypes from '../actions/actionTypes';

const initialState = {
	company: null,
	token: null
}

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.AUTH_COMPANY: {
			return {
				...state,
				token: action.token,
				company: action.company
			}
		}
		case actionTypes.UPDATE_COMPANY_DATA: {
			return {
				...state,
				company: action.company
			}
		}
		case actionTypes.LOGOUT_COMPANY: {
			return {
				...state,
				company: null,
				token: null
			}
		}
		default:
			return state;
	}
}

export default reducer;