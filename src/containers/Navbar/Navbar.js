import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import { MDBBtn, MDBIcon } from 'mdbreact';
import styles from './Navbar.module.scss';
import Cookies from 'js-cookie';

import { connect } from 'react-redux';
//images
import logo from '../../image/logo.png'

import * as actionCreators from '../../store/actions/index';

import ModalLogin from './ModalLogin/ModalLogin';
import ModalRegister from './ModalRegister/ModalRegister'

class Navbar extends Component {
    state = {
        smallMenuShow: false,
        smallMenuAnimation: " ",
        registerModalShow: false,
        loginModalShow: false
    }

    showSmallMenu = () => {
        if (this.state.smallMenuShow) {
            this.setState({ smallMenuAnimation: " animated fadeOutUp" })
            setTimeout(() => { this.setState({ smallMenuShow: false }) }, 500);
        } else {
            this.setState({
                smallMenuShow: true,
                smallMenuAnimation: " animated fadeInDown"
            })
        }
    }

    login = () => {
        this.setState({ loginModalShow: true })
    }

    register = () => {
        this.setState({ registerModalShow: true })
    }

    logout = () => {
        Cookies.remove("userToken");
        Cookies.remove("organizationToken");
        this.props.logoutCompany();
        this.props.logoutUser();
        this.props.history.push("/")

    }

    toggleModal = () => {
        this.setState({ loginModalShow: false, registerModalShow: false })
    }

    //navbar functions
    account = () => {
        if(Cookies.get("userToken"))
            this.props.history.push("/user")
        else if(Cookies.get("organizationToken"))
            this.props.history.push("/company")
        else
            this.setState({ loginModalShow: true })
    }

    redirectToMain = () => {
        this.props.history.push("/")
    }

    render() {
        return (
            <>
                <nav className={styles.navbarMain}>
                    <div className={styles.logoDiv} onClick={this.redirectToMain}>
                        <img className={styles.logo} src={logo} alt="logo" />
                    </div>
                    <div className={styles.navigationDivSmall}>
                        <MDBBtn color="unique" onClick={this.showSmallMenu}>
                            <MDBIcon icon="fas fa-bars" />
                        </MDBBtn>
                    </div>
                    <div className={styles.navigationDiv}>
                        <div className={styles.linksDiv}>
                            <span className={styles.link} onClick={()=>  this.props.history.push("/")}>OFERTY</span>
                            <span className={styles.link} onClick={this.account}>TWOJE KONTO</span>
                            <span className={styles.link}>O NAS</span>
                        </div>

                        {Cookies.get("userToken") || Cookies.get("organizationToken") ?
                            <div className={styles.accountDiv}>
                                <MDBBtn color="red" size="sm" onClick={this.logout}>Wyloguj</MDBBtn>
                            </div>
                            :
                            <div className={styles.accountDiv}>
                                <MDBBtn color="unique" size="sm" onClick={this.login}>Zaloguj</MDBBtn>
                                <MDBBtn color="unique" size="sm" onClick={this.register}>Zarejestruj</MDBBtn>
                            </div>
                        }

                    </div>
                </nav>
                {(this.state.smallMenuShow) ?
                    <div className={styles.smallMenu + this.state.smallMenuAnimation}>
                        <span className={styles.smallLink}>OFERTY</span>
                        <span className={styles.smallLink} onClick={this.account}>TWOJE KONTO</span>
                        <span className={styles.smallLink}>O NAS</span>
                        {Cookies.get("userToken") || Cookies.get("organizationToken") ?
                            <div className={styles.accountDivSmall}>
                                <MDBBtn color="red" size="sm" onClick={this.logout}>Wyloguj</MDBBtn>
                            </div>
                            :
                            <div className={styles.accountDivSmall}>
                                <MDBBtn color="unique" size="sm" onClick={this.login}>Zaloguj</MDBBtn>
                                <MDBBtn color="unique" size="sm" onClick={this.register}>Zarejestruj</MDBBtn>
                            </div>
                        }
                    </div> : null}

                {(this.state.loginModalShow) ? <ModalLogin toggleModal={this.toggleModal} modalShow={this.state.loginModalShow} /> : null}
                {(this.state.registerModalShow) ? <ModalRegister toggleModal={this.toggleModal} modalShow={this.state.registerModalShow} /> : null}
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.user.token !== null,
        user: state.user.user,
        company: state.company.company
    };
};

const mapDispatchToProps = dispatch => {
    return {
        logoutUser: () => dispatch(actionCreators.logoutUser()),
        logoutCompany: () => dispatch(actionCreators.logoutCompany())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Navbar));