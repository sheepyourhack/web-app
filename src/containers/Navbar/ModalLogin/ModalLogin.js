import React, { Component } from 'react';
import { MDBModal, MDBModalHeader, MDBModalBody } from 'mdbreact';
import Form from '../../../components/Form/Form';
import styles from './ModalLogin.module.scss';
import loginFields from './loginFields';
import { connect } from 'react-redux';
import Cookies from 'js-cookie';

import * as actionCreators from '../../../store/actions/index';

import deepCopy from 'deepcopy';
import { checkValidity } from '../../../helpers/helpers';
import axios from 'axios';

class ModalLogin extends Component {
    state = {
        fields: loginFields,
        showError: false,
    }

    inputChangedHandler = async (e, fieldName) => {
        const fields = deepCopy(this.state.fields);

        fields[fieldName].value = e.target.value;
        fields[fieldName].touched = true;
        fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);

        this.setState({ fields });
        // console.log(fields[fieldName])
    }

    isFormValid = () => {
        for (const field in this.state.fields) {
            if (!this.state.fields[field].validation.valid) return false;
        }

        return true;
    }

    sendData = async () => {
        this.setState({ showError: true })
        if (this.isFormValid()) {
            const updateObject = {}
            for (const key in this.state.fields) {
                updateObject[key] = this.state.fields[key].value;
            }

            const res = await axios.post(`/api/auth/login`, updateObject);
            console.log(res)
            if (res.data.organization) {
                this.props.authCompany(res.data.token, res.data.organization);
                Cookies.set("organizationToken", res.data.token);
                this.props.toggleModal();
            } else if (res.data.user) {
                this.props.authUser(res.data.token, res.data.user);
                Cookies.set("userToken", res.data.token);
                this.props.toggleModal();
            }
        }
    }


    render() {
        return (
            <MDBModal toggle={this.props.toggleModal} size="lg" isOpen={this.props.modalShow} centered>
                <MDBModalHeader toggle={this.props.toggleModal}>Login</MDBModalHeader>
                <MDBModalBody>
                    <Form
                        style={{ alignContent: 'center', }}
                        fields={this.state.fields}
                        inputChangedHandler={this.inputChangedHandler}
                        showError={this.state.showError}
                        clickHandler={() => this.sendData()}
                        buttonText={"LOGIN"}
                        button={true} />
                    {this.state.serverErrorMessage ? <p className={styles.serverErrorMessage}>{this.state.serverErrorMessage}</p> : null}
                </MDBModalBody>
            </MDBModal>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.user.token !== null,
        user: state.user.user,
        company: state.company.company
    };
};

const mapDispatchToProps = dispatch => {
    return {
        authUser: (token, user) => dispatch(actionCreators.authUser(token, user)),
        authCompany: (token, company) => dispatch(actionCreators.authCompany(token, company))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalLogin);