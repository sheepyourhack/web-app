export default {
    email: {
        elementType: 'input',
        inputType: 'text',
        icon: 'user',
        value: '',
        label: 'email',
        validationRules: {
            required: true,
            minLength: 5,
            maxLength: 15,
        },
        validation: {
            valid: false,
            error: 'Pole nie może być puste',
        }
    },
    password: {
        elementType: 'input',
        inputType: 'password',
        icon: 'lock',
        value: '',
        label: 'Hasło',
        validationRules: {
            required: true,
            minLength: 5,
            maxLength: 15,
        },
        validation: {
            valid: false,
            error: 'Pole nie może być puste',
        },
    }
}