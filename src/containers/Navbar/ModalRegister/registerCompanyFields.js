export default {
    name: {
        elementType: 'input',
        inputType: 'text',
        icon: 'user',
        value: '',
        label: 'Nazwa organizacji',
        validationRules: {
            required: true,
            minLength: 5,
            maxLength: 15,
        },
        validation: {
            valid: false,
            error: 'Pole nie może być puste',
        }
    },
    password: {
        elementType: 'input',
        inputType: 'password',
        icon: 'lock',
        value: '',
        label: 'Hasło',
        validationRules: {
            required: true,
            minLength: 5,
            maxLength: 15,
        },
        validation: {
            valid: false,
            error: 'Pole nie może być puste',
        },
    },
    email: {
        elementType: 'input',
        inputType: 'email',
        icon: 'envelope',
        value: '',
        label: 'E-mail',
        validationRules: {
            required: true,
            isEmail: true
        },
        validation: {
            valid: false,
            error: 'Pole nie może być puste',
        },
        touched: false,
    }
}