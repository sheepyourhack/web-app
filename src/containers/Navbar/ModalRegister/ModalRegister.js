import React, { Component } from 'react';
import { MDBModal, MDBModalHeader, MDBModalBody, MDBIcon } from 'mdbreact';
import styles from './ModalRegister.module.scss';
import deepCopy from 'deepcopy';
import { checkValidity } from '../../../helpers/helpers';
import axios from 'axios';
import Form from '../../../components/Form/Form';

import registerCompanyFields from './registerCompanyFields';
import registerUserFields from './registerUserFields';

//images
import user from '../../../image/p2.png'
import company from '../../../image/f1.png'

class ModalRegister extends Component {
    state = {
        type: '',
        fields: null,
        showError: false,
        fieldsArr: [
            { name: "UŻYTKOWNIK", icon: user },
            { name: "FIRMA", icon: company },
        ]
    }

    inputChangedHandler = async (e, fieldName) => {
        const fields = deepCopy(this.state.fields);

        fields[fieldName].value = e.target.value;
        fields[fieldName].touched = true;
        fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);

        this.setState({ fields });
    }

    isFormValid = () => {
        for (const field in this.state.fields) {
            if (!this.state.fields[field].validation.valid) return false;
        }

        return true;
    }

    sendData = async () => {
        this.setState({ showError: true })
        if (this.isFormValid()) {
            const updateObject = {}
            for (const key in this.state.fields) {
                updateObject[key] = this.state.fields[key].value;
            }

            if (this.state.type === "company") {
                const res = await axios.post(`/api/organization/add`, updateObject);
                console.log(res);
            } else if (this.state.type === "user") {
                const res = await axios.post(`/api/user/add`, updateObject);
                console.log(res);
            }
        }
    }
    chooseCategory = (e) => {
		console.log(e.currentTarget.getAttribute('data-value'))
        if (e.currentTarget.getAttribute('data-value') === 'FIRMA') {
            this.setState({ type: 'company', fields: registerCompanyFields })
        } else {
            this.setState({ type: 'user', fields: registerUserFields })
        }
    }

    render() {
        return (
            <MDBModal toggle={this.props.toggleModal} size="lg" isOpen={this.props.modalShow} centered>
                <MDBModalHeader toggle={this.props.toggleModal}>REGISTER</MDBModalHeader>
                <MDBModalBody>
                    {
                        (this.state.type === '') ?
                            <div className={styles.modalFieldsType}>
                                {this.state.fieldsArr.map((el, i) => {
                                    return (
                                        <div key={i} onClick={this.chooseCategory} data-value={el.name} className={styles.modalFieldsChoose}>
                                            <div>
                                                <img src={el.icon} style={{width: "63px", height: "auto"}} alt="icon"/>    
                                            </div>
                                            <div>{el.name}</div>
                                        </div>
                                    )
                                })}
                            </div>
                            :
                            <div>
                                <Form
                                    style={{ alignContent: 'center', }}
                                    fields={this.state.fields}
                                    inputChangedHandler={this.inputChangedHandler}
                                    showError={this.state.showError}
                                    clickHandler={() => this.sendData()}
                                    buttonText={"REGISTER"}
                                    button={true} />
                                {this.state.serverErrorMessage ? <p className={styles.serverErrorMessage}>{this.state.serverErrorMessage}</p> : null}
                            </div>
                    }
                </MDBModalBody>
            </MDBModal>
        );
    }
}

export default ModalRegister;