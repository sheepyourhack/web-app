export default {
    email: {
        elementType: 'input',
        inputType: 'email',
        icon: 'envelope',
        value: '',
        label: 'E-mail',
        validationRules: {
            required: true,
            isEmail: true
        },
        validation: {
            valid: false,
            error: 'Pole nie może być puste',
        },
        touched: false,
    },
    password: {
        elementType: 'input',
        inputType: 'password',
        icon: 'lock',
        value: '',
        label: 'Hasło',
        validationRules: {
            required: true,
            minLength: 5,
            maxLength: 15,
        },
        validation: {
            valid: false,
            error: 'Pole nie może być puste',
        },
    },
    firstName: {
        elementType: 'input',
        inputType: 'text',
        icon: 'user',
        value: '',
        label: 'Imie',
        validationRules: {
            required: true,
            minLength: 5,
            maxLength: 25,
        },
        validation: {
            valid: false,
            error: 'Pole nie może być puste',
        }
    },
    lastName: {
        elementType: 'input',
        inputType: 'text',
        icon: 'user',
        value: '',
        label: 'Nazwisko',
        validationRules: {
            required: true,
            minLength: 5,
            maxLength: 25,
        },
        validation: {
            valid: false,
            error: 'Pole nie może być puste',
        }
    },
    birthDate: {
        elementType: 'input',
        inputType: 'date',
        icon: 'calendar-alt',
        value: '',
        label: 'Data urodzenia',
        validationRules: {
            required: true,
            minLength: 5,
            maxLength: 15,
        },
        validation: {
            valid: false,
            error: 'Pole nie może być puste',
        }
    },
}