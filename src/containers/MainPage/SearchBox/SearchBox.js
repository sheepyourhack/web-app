import React, { Component } from 'react';
import styles from './SearchBox.module.scss';
import axios from 'axios';
import Cookies from 'js-cookie';
import { MDBInput, MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBContainer } from 'mdbreact';
class SearchBox extends Component {
    state = {
        title: "",
        city: "",
        practice: true,
        work: true,
        volunteer: true,
        page: 1,
        offers: []
    }
    componentDidMount = () => {
        this.onClick();
    }
    onClick = async () => {
        const res = await axios.post('/api/offer', { searchText: this.state.title, city: this.state.city, page: this.state.page, type: this.getTypeArray(), ownerType: this.getOwnerType() });
        console.log(res.data);
        this.setState({ offers: res.data.offers.docs });
    }
    getTypeArray = () => {
        const arr = [];
        if (this.state.work) arr.push("staż", "Staż");
        if (this.state.volunteer) arr.push("wolontariat", "Wolontariat");
        if (this.state.practice) arr.push("praktyka", "Praktyka");
        return arr;
    }
    getOwnerType = () => {
        if (Cookies.get("organizationToken")) return "Użytkownik"
        else return "Organizacja";
    }
    render() {
        return (
            <div>
                <div className={styles.searchBox}>
                    <div className={styles.inputs}>
                        <MDBInput className={styles.searchInput} onChange={(e) => this.setState({ title: e.target.value })} outline hint='Tytuł'></MDBInput>
                        <MDBInput className={styles.searchInput} onChange={(e) => this.setState({ city: e.target.value })} outline hint='Miasto'></MDBInput>
                    </div>
                    <div className={styles.containerFull}>
                        <div className={styles.checkboxContainer}>
                            <div className={styles.container}><MDBInput onChange={(e) => this.setState({ practice: !this.state.practice })} className={styles.checkbox} checked={this.state.practice} type="checkbox" id="checkbox1" /> <div>Praktyka</div></div>
                            <div className={styles.container}><MDBInput onChange={(e) => this.setState({ volunteer: !this.state.volunteer })} className={styles.checkbox} checked={this.state.volunteer} type="checkbox" id="checkbox2" /><div>Wolontariat</div></div>
                            <div className={styles.container}><MDBInput onChange={(e) => this.setState({ work: !this.state.work })} className={styles.checkbox} checked={this.state.work} type="checkbox" id="checkbox3" /><div>Staż</div></div>
                        </div>
                        <div className={styles.buttonContainer}>
                            <MDBBtn size='sm' onClick={this.onClick} color='unique'>Szukaj</MDBBtn>
                        </div>
                    </div>
                </div >
                <MDBContainer>
                    <div className={styles.offers}>
                        {this.state.offers.map(offer => {
                            return (<MDBCard style={{ width: "23rem", marginBottom: "60px" }}>
                                <MDBCardImage className="img-fluid" src={offer.imagePath ? `/api/image/${offer.imagePath}` : "https://www.mvcc.vic.gov.au/-/media/Images/Content/Hero-images/Explore/volunteer-hero.ashx"} waves />
                                <MDBCardBody>
                                    <MDBCardTitle>{offer.title}</MDBCardTitle>
                                    <MDBCardText>
                                        {offer.description}
                                    </MDBCardText>
                                    <MDBBtn color="unique" size="sm" href="#">Szczegóły</MDBBtn>
                                    <span style={{marginLeft: "10px"}}>{offer.startDate && offer.endDate ? `${new Date(offer.startDate).toLocaleDateString()} - ${new Date(offer.endDate).toLocaleDateString()}` : "" }</span>
                                </MDBCardBody>
                            </MDBCard>)
                        })}
                    </div>
                </MDBContainer>
            </div >);
    }
}

export default SearchBox;