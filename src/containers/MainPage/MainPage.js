import React, { Component } from 'react';
import SearchBox from './SearchBox/SearchBox'
import styles from './MainPage.module.scss';

export default class MainPage extends Component {
    render() {
        return (
            <div>
                <SearchBox />
            </div>
        )
    }
}
