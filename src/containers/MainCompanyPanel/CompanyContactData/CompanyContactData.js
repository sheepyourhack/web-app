import React, { Component } from 'react';
import Form from '../../../components/Form/Form';
import deepCopy from 'deepcopy';
import { checkValidity } from '../../../helpers/helpers';
import axios from 'axios';
import { connect } from 'react-redux';
import * as actionCreators from '../../../store/actions/index';
import Cookies from 'js-cookie';

import styles from '../CompanyInfo/CompanyInfo.module.scss';
import companyContactDataFields from '../CompanyFields/companyContactDataFields';
const addInput = {
    elementType: 'outlineInput',
    inputType: 'text',
    icon: 'building',
    value: '',
    label: 'Czym zajmuje się firma?',
    add: false,
    validationRules: {
        required: false,
    },
    validation: {
        valid: true
    },
}
class CompanyContactData extends Component {
    state = {
        fields: null,
        filled: null,
        values: [],
        specializationFields: {

        },
        number: 0,
    }

    componentDidMount() {
        this.setState({ fields: companyContactDataFields });
        let spec = deepCopy(this.state.specializationFields);
        spec['field' + this.state.number] = addInput;
        let num = deepCopy(this.state.number);
        num++;
        this.setState({ specializationFields: spec, number: num });
    }

    // fill fields with info only of first redux state change
    static getDerivedStateFromProps(props, state) {
        if (!state.filled && props.company && state.fields) {
            const specializations = {};
            let number = 0;

            for (const key in state.fields) {
                state.fields[key].value = props.company[key]
                state.fields[key].validation = { valid: true }
            }

            props.company.specializations.forEach(specialization => {
                specializations['field' + number] = deepCopy(addInput);
                specializations['field' + number].value = specialization;
                specializations['field' + number].add = true;
                number++;
            });

            specializations['field' + number] = deepCopy(addInput);
            specializations['field' + number].value = "";
            specializations['field' + number].add = false;

            state.specializationFields = specializations;
            state.number = number + 1;

            state.filled = true;
        }

        return state;
    }

    isFormValid = () => {
        for (const field in this.state.fields) {
            if (!this.state.fields[field].validation.valid) return false;
        }

        return true;
    }

    inputChangedHandler = async (e, fieldName) => {
        const fields = deepCopy(this.state.fields);
        fields[fieldName].value = e.target.value;
        fields[fieldName].touched = true;
        fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);
        this.setState({ fields: fields });
    }

    inputChangedHandlerSpecialization = (e, fieldName) => {
        const fields = deepCopy(this.state.specializationFields);
        fields[fieldName].value = e.target.value;
        fields[fieldName].touched = true;
        fields[fieldName].add = true;
        fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);
        this.setState({ specializationFields: fields });
    }

    addNewInput = (e, fieldName) => {
        if (!this.state.specializationFields[fieldName].add) {
            let spec = deepCopy(this.state.specializationFields);
            spec['field' + this.state.number] = addInput;
            let num = deepCopy(this.state.number);
            num++;
            spec[fieldName].add = true;
            this.setState({ specializationFields: spec, number: num, });
        }
    }


    sendData = async () => {
        this.setState({ showError: true });
        if (this.isFormValid()) {
            const updateObject = {}
            const updateObjectS = []
            for (const key in this.state.fields) {
                updateObject[key] = this.state.fields[key].value
            }
            for (const key in this.state.specializationFields) {
                updateObjectS.push(this.state.specializationFields[key].value)
            }
            for (let i = 0; i < updateObjectS.length; i++) {
                if (updateObjectS[i] === '') {
                    updateObjectS.splice(i, i + 1)
                }
            }
            updateObject.specializations = updateObjectS;
            const res = await axios.patch(`/api/organization/${this.props.company._id}`, updateObject);
            console.log(res);

            this.props.authCompany(Cookies.get("organizationCookie"), res.data.organization);

            const specializations = {};
            let number = 0;

            this.props.company.specializations.forEach(specialization => {
                specializations['field' + number] = deepCopy(addInput);
                specializations['field' + number].value = specialization;
                specializations['field' + number].add = true;
                number++;
            });

            specializations['field' + number] = deepCopy(addInput);
            specializations['field' + number].value = "";
            specializations['field' + number].add = false;

            number++;

            this.setState({ specializationFields: specializations, number });
        }
    }

    render() {
        return (
            <div className={styles.info}>
                {this.state.fields ?
                    <>
                        <div className={styles.mainDiv}>
                            <div className={styles.infoDiv}>
                                <Form
                                    fields={this.state.fields}
                                    inputChangedHandler={this.inputChangedHandler}
                                    showError={this.state.showError}
                                    clickHandler={this.sendData}
                                    buttonText="Zapisz dane"
                                    button={false}
                                />
                                <Form
                                    fields={this.state.specializationFields}
                                    inputChangedHandler={this.inputChangedHandlerSpecialization}
                                    showError={this.state.showError}
                                    clickHandler={this.sendData}
                                    addNewInput={this.addNewInput}
                                    buttonText="Zapisz dane"
                                    button={true}
                                />
                            </div>
                        </div>
                    </> : null}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.user.token !== null,
        company: state.company.company
    };
};

const mapDispatchToProps = dispatch => {
    return {
        authCompany: (token, company) => dispatch(actionCreators.authCompany(token, company))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CompanyContactData);