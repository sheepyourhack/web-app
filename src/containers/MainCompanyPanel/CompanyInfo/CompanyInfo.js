import React, { Component } from 'react';
// import { connect } from 'react-redux';
import Form from '../../../components/Form/Form';
import deepCopy from 'deepcopy';
import { checkValidity } from '../../../helpers/helpers';
import axios from 'axios';
import { connect } from 'react-redux';
import * as actionCreators from '../../../store/actions/index';
import Cookies from 'js-cookie';

import styles from './CompanyInfo.module.scss';
import companyInfoFields from '../CompanyFields/companyInfoFields';

// import * as actionCreators from '../../../store/actions/index';

class CompanyInfo extends Component {
	state = {
		fields: null,
		filled: false
	}

	componentDidMount() {
		this.setState({ fields: companyInfoFields });
	}

	// fill fields with info only of first redux state change
	static getDerivedStateFromProps(props, state) {
		if (!state.filled && props.company && state.fields) {

			for (const key in state.fields) {
				if (key === "foundingDate") {
					state.fields[key].value = new Date(props.company[key]).toISOString().substring(0, 10);
					state.fields[key].validation = { valid: true }
				} else {
					state.fields[key].value = props.company[key]
					state.fields[key].validation = { valid: true }
				}
			}

			state.filled = true;
		}
		return state;
	}

	isFormValid = () => {
		for (const field in this.state.fields) {
			if (!this.state.fields[field].validation.valid) return false;
		}

		return true;
	}

	inputChangedHandler = async (e, fieldName) => {
		const fields = deepCopy(this.state.fields);
		fields[fieldName].value = e.target.value;
		fields[fieldName].touched = true;
		fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);
		this.setState({ fields: fields });
	}


	sendData = async () => {
		this.setState({ showError: true });
		if (this.isFormValid()) {
			const updateObject = {}
			for (const key in this.state.fields) {
				updateObject[key] = this.state.fields[key].value
			}

			const res = await axios.patch(`/api/organization/${this.props.company._id}`, updateObject);

			this.props.authCompany(Cookies.get("userToken"), res.data.organization);
		}
	}

	sendFile = async (image) => {
		if (image) {
			const formData = new FormData();
			formData.append('image', image, image.name);

			const res = await axios({
				url: '/api/image/add',
				method: 'POST',
				headers: { 'Authorization': `Bearer ${this.props.token}`, 'Content-Type': 'multipart/form-data' },
				data: formData
			});

			if (!res.data.error) {
				const fields = this.state.fields;
				fields.imagePath.value = res.data.imagePath;
				fields.imagePath.valid = { valid: true }
				this.setState({ fields });
			}
		}
	}

	render() {
		return (
			<div className={styles.info}>
				{this.state.fields ?
					<>
						<div className={styles.mainDiv}>
							<div className={styles.infoDiv}>
								<Form
									fields={this.state.fields}
									inputChangedHandler={this.inputChangedHandler}
									showError={this.state.showError}
									clickHandler={this.sendData}
									buttonText="Zapisz dane"
									button={true}
								/>
							</div>
							<div className={styles.imgDiv}>
								{this.state.fields.imagePath.value ? <img src={`/api/image/${this.state.fields.imagePath.value}`} className={styles.logo} alt="logo" /> : null}

								<label className={styles.label} htmlFor="upload-photo">Wybierz zdjęcie...</label>
								<input className={styles.inputFile} id="upload-photo" type="file" onChange={(e) => this.sendFile(e.target.files[0])} />
							</div>
						</div>
					</> : null}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		company: state.company.company
	};
};

const mapDispatchToProps = dispatch => {
	return {
		authCompany: (token, company) => dispatch(actionCreators.authCompany(token, company))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CompanyInfo);