import React, { Component } from 'react';
import Form from '../../../components/Form/Form';
import deepCopy from 'deepcopy';
import { checkValidity } from '../../../helpers/helpers';
import axios from 'axios';
import { connect } from 'react-redux';
import * as actionCreators from '../../../store/actions/index';
import Cookies from 'js-cookie';
import { MDBInput, MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBContainer, MDBIcon } from 'mdbreact';

import styles from '../CompanyInfo/CompanyInfo.module.scss';

class CompanyEditOffer extends Component {
    state = {
        offers: null,
    }

    componentDidMount = async () => {
        console.log(this.props)
        const res = await axios.post('/api/offer/organization', { "ownerId": this.props.company._id });
        console.log(res.data);
        this.setState({ offers: res.data.offers.docs });
    }

    isFormValid = () => {
        for (const field in this.state.fields) {
            if (!this.state.fields[field].validation.valid) return false;
        }

        return true;
    }

    // inputChangedHandler = async (e, fieldName) => {
    //     const fields = deepCopy(this.state.fields);
    //     fields[fieldName].value = e.target.value;
    //     fields[fieldName].touched = true;
    //     fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);
    //     this.setState({ fields: fields });
    // }

    // inputChangedHandlerSpecialization = (e, fieldName) => {
    //     const fields = deepCopy(this.state.specializationFields);
    //     fields[fieldName].value = e.target.value;
    //     fields[fieldName].touched = true;
    //     fields[fieldName].add = true;
    //     fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);
    //     this.setState({ specializationFields: fields });
    // }


    // sendData = async () => {
    //     this.setState({ showError: true });
    //     if (this.isFormValid()) {
    //         const updateObject = {}
    //         const updateObjectS = []
    //         for (const key in this.state.fields) {
    //             updateObject[key] = this.state.fields[key].value
    //         }
    //         for (const key in this.state.specializationFields) {
    //             updateObjectS.push(this.state.specializationFields[key].value)
    //         }
    //         for (let i = 0; i < updateObjectS.length; i++) {
    //             if (updateObjectS[i] === '') {
    //                 updateObjectS.splice(i, i + 1)
    //             }
    //         }
    //         updateObject.specializations = updateObjectS;
    //         const res = await axios.patch(`/api/organization/${this.props.company._id}`, updateObject);
    //         console.log(res);

    //         this.props.authCompany(Cookies.get("organizationCookie"), res.data.organization);

    //     }
    // }

    render() {
        return (
            <div className={styles.info}>
                {this.state.offers ?
                    <MDBContainer style={{position: 'relative'}}>
                        <div className={styles.offers} style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-around' }}>
                            {this.state.offers.map(offer => {
                                return (<MDBCard style={{ width: "23rem", marginBottom: "60px" }}>
                                    <MDBCardImage className="img-fluid" src={offer.imagePath ? `/api/image/${offer.imagePath}` : "https://www.mvcc.vic.gov.au/-/media/Images/Content/Hero-images/Explore/volunteer-hero.ashx"} waves />
                                    <MDBCardBody>
                                        <MDBCardTitle>{offer.title}</MDBCardTitle>
                                        <div style={{position: 'relative', bottom: '1%'}}> <MDBBtn color="unique" size="md" href="#">Edytuj</MDBBtn>
                                            <MDBBtn outline color="danger" size="md">Usuń</MDBBtn></div>
                                    </MDBCardBody>
                                </MDBCard>)
                            })}
                        </div>
                    </MDBContainer>
                    :
                    null}
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.user.token !== null,
        company: state.company.company
    };
};

const mapDispatchToProps = dispatch => {
    return {
        authCompany: (token, company) => dispatch(actionCreators.authCompany(token, company))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CompanyEditOffer);