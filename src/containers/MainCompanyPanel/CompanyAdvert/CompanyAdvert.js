import React, { Component } from 'react';
// import { connect } from 'react-redux';
import Form from '../../../components/Form/Form';
import deepCopy from 'deepcopy';
import { checkValidity } from '../../../helpers/helpers';
import axios from 'axios';

import { connect } from 'react-redux';
import * as actionCreators from '../../../store/actions/index';

import ResFields from './ResFields'
import styles from '../CompanyInfo/CompanyInfo.module.scss';
import companyAdvertFields from '../CompanyFields/companyAdvertFields';

// import * as actionCreators from '../../../store/actions/index';
const requirements = {
	elementType: 'outlineInput',
	inputType: 'text',
	icon: 'fas fa-pencil-alt',
	value: '',
	label: 'Obowiązki',
	add: false,
	validationRules: {
		required: false,
	},
	validation: {
		valid: true
	},
};
class CompanyAdvert extends Component {
	state = {
		fields: null,
		filled: false,
		requirementsFields: {},
		responsibilitiesFields: {},
		reqNumber: 0,
		resNumber: 0,
	}

	componentDidMount() {
		this.setState({ fields: companyAdvertFields });
		let spec = deepCopy(this.state.requirementsFields);
		spec['field' + this.state.reqNumber] = requirements;
		let reqNumber = deepCopy(this.state.reqNumber);
		reqNumber++;
		this.setState({ requirementsFields: spec, reqNumber: reqNumber });
	}

	isFormValid = () => {
		for (const field in this.state.fields) {
			if (!this.state.fields[field].validation.valid) return false;
		}

		return true;
	}

	inputChangedHandler = async (e, fieldName) => {
		const fields = deepCopy(this.state.fields);
		fields[fieldName].value = e.target.value;
		fields[fieldName].touched = true;
		fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);
		this.setState({ fields: fields });
	}

	inputChangedHandlerReq = (e, fieldName) => {
		const fields = deepCopy(this.state.requirementsFields);
		fields[fieldName].value = e.target.value;
		fields[fieldName].touched = true;
		fields[fieldName].add = true;
		fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);
		this.setState({ requirementsFields: fields });
	}

	sendData = async () => {
		this.setState({ showError: true });
		if (this.isFormValid()) {
			const updateObject = {}
			const updateObjectS = []
			const updateObjectSa = []
			for (const key in this.state.fields) {
				updateObject[key] = this.state.fields[key].value
			}
			for (const key in this.state.responsibilitiesFields) {
				updateObjectS.push(this.state.responsibilitiesFields[key].value)
			}
			for (let i = 0; i < updateObjectS.length; i++) {
				if (updateObjectS[i] == '') {
					updateObjectS.splice(i, i + 1)
				}
			}
			updateObject.responsibilities = updateObjectSa;

			for (const key in this.state.requirementsFields) {
				updateObjectSa.push(this.state.requirementsFields[key].value)
			}
			for (let i = 0; i < updateObjectSa.length; i++) {
				if (updateObjectSa[i] == '') {
					updateObjectSa.splice(i, i + 1)
				}
			}
			updateObject.requirements = updateObjectS;
			updateObject.ownerName = this.props.company.name;
			updateObject.ownerId = this.props.company._id;
			updateObject.ownerType = "Organizacja";
			updateObject.paid = updateObject.paid === "Płatny" ? true : false;

			const res = await axios.post(`/api/offer/add`, updateObject);
			console.log(res);
		}
	}

	addNewInput = (e, fieldName) => {
		if (!this.state.requirementsFields[fieldName].add) {
			let spec = deepCopy(this.state.requirementsFields);
			spec['field' + this.state.reqNumber] = requirements;
			let reqNumber = deepCopy(this.state.reqNumber);
			reqNumber++;
			this.setState({ requirementsFields: spec, reqNumber: reqNumber });
		}
	}

	addNewRes = (n) => {
		this.setState({ responsibilitiesFields: n });
	}
	editRes = (n) => {
		this.setState({ responsibilitiesFields: n });
	}

	changeSelect = (e, fieldName) => {
		// console.log(e.target.value)
		const fields = deepCopy(this.state.fields);
		fields[fieldName].value = e.target.value;
		this.setState({ fields: fields });
	}

	sendFile = async (image) => {
		if (image) {
			const formData = new FormData();
			formData.append('image', image, image.name);

			const res = await axios({
				url: '/api/image/add',
				method: 'POST',
				headers: { 'Authorization': `Bearer ${this.props.token}`, 'Content-Type': 'multipart/form-data' },
				data: formData
			});

			if (!res.data.error) {
				const fields = this.state.fields;
				fields.imagePath.value = res.data.imagePath;
				fields.imagePath.valid = { valid: true }
				this.setState({ fields });
			}
		}
	}

	render() {
		return (
			<div className={styles.info}>
				{this.state.fields ?
					<>
						<div className={styles.mainDiv}>
							<div className={styles.infoDiv}>
								<Form
									fields={this.state.fields}
									inputChangedHandler={this.inputChangedHandler}
									showError={this.state.showError}
									clickHandler={this.sendData}
									buttonText="Zapisz dane"
									button={false}
									changeSelect={this.changeSelect}
								/>
								<ResFields add={this.addNewRes} edit={this.editRes} />
								<Form
									fields={this.state.requirementsFields}
									inputChangedHandler={this.inputChangedHandlerReq}
									showError={this.state.showError}
									clickHandler={this.sendData}
									buttonText="Zapisz dane"
									button={true}
									addNewInput={this.addNewInput}
								/>
							</div>
							<div className={styles.imgDiv}>
								{this.state.fields.imagePath.value ? <img src={`/api/image/${this.state.fields.imagePath.value}`} className={styles.logo} alt="logo" /> : null}

								<label className={styles.label} htmlFor="upload-photo">Wybierz zdjęcie...</label>
								<input className={styles.inputFile} id="upload-photo" type="file" onChange={(e) => this.sendFile(e.target.files[0])} />
							</div>
						</div>
					</> : null}
			</div>
		);
	}
}


const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		company: state.company.company
	};
};

const mapDispatchToProps = dispatch => {
	return {
		authCompany: (token, company) => dispatch(actionCreators.authCompany(token, company))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CompanyAdvert);