import React, { Component } from 'react';
// import { connect } from 'react-redux';
import Form from '../../../components/Form/Form';
import deepCopy from 'deepcopy';
import { checkValidity } from '../../../helpers/helpers';
// import axios from 'axios';

import styles from '../CompanyInfo/CompanyInfo.module.scss';
import companyAdvertFields from '../CompanyFields/companyAdvertFields';

// import * as actionCreators from '../../../store/actions/index';
const responsibilities = {
    elementType: 'outlineInput',
    inputType: 'text',
    icon: 'fas fa-pencil-alt',
    value: '',
    label: 'Wymagane umiejętności',
    add: false,
    validationRules: {
        required: false,
    },
    validation: {
        valid: true
    },
};

class ResFields extends Component {
    state = {
        fields: null,
        filled: false,
        responsibilitiesFields: {},
        resNumber: 0,
    }

    componentDidMount() {
        let spec2 = deepCopy(this.state.responsibilitiesFields);
        spec2['field' + this.state.resNumber] = responsibilities;
        let resNumber = deepCopy(this.state.resNumber);
        resNumber++;
        this.setState({ responsibilitiesFields: spec2, resNumber: resNumber });
    }

    inputChangedHandlerRes = (e, fieldName) => {
        const fields = deepCopy(this.state.responsibilitiesFields);
        fields[fieldName].value = e.target.value;
        fields[fieldName].touched = true;
        fields[fieldName].add = true;
        fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);
        console.log(fields[fieldName].add)
        this.setState({ responsibilitiesFields: fields });
        this.props.edit(fields)
    }


    addNewInput = (e, fieldName) => {
        console.log(fieldName, this.state.responsibilitiesFields[fieldName].add)
        if (!this.state.responsibilitiesFields[fieldName].add) {
            let spec = deepCopy(this.state.responsibilitiesFields);
            spec['field' + this.state.resNumber] = responsibilities;
            let resNumber = deepCopy(this.state.resNumber);
            resNumber++;
            this.setState({ responsibilitiesFields: spec, resNumber: resNumber });
            this.props.add(spec);
        }
    }

    render() {
        return (
            <Form
                fields={this.state.responsibilitiesFields}
                inputChangedHandler={this.inputChangedHandlerRes}
                showError={this.state.showError}
                clickHandler={this.sendData}
                buttonText="Zapisz dane"
                button={false}
                addNewInput={this.addNewInput}
            />
        );
    }
}

export default ResFields;