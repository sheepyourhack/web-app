
const companyContactDataFields = {
    address: {
        elementType: 'outlineInput',
        inputType: 'text',
        icon: 'map-marker-alt ',
        value: '',
        label: 'Adres',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    webpage: {
        elementType: 'outlineInput',
        inputType: 'text',
        icon: 'globe',
        value: '',
        label: 'Strona internetowa',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    size: {
        elementType: 'outlineInput',
        inputType: 'text',
        icon: 'users',
        value: '',
        label: 'Liczba pracowników',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    type: {
        elementType: 'outlineInput',
        inputType: 'text',
        icon: 'building',
        value: '',
        label: 'Typ organizacji',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    // specialization: {
    //     elementType: 'outlineInput',
    //     inputType: 'text',
    //     icon: 'building',
    //     value: '',
    //     label: 'Czym zajmuje się firma?',
    //     validationRules: {
    //         required: false,
    //     },
    //     validation: {
    //         valid: true
    //     },
    // },
}

export default companyContactDataFields;
