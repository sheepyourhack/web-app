
const companyInfoFields = {
	branch: {
		elementType: 'outlineInput',
		inputType: 'text',
		icon: 'fas fa-pencil-alt',
		value: '',
		label: 'Branża firmy',
		validationRules: {
			required: false,
		},
		validation: {
			valid: true
		},
	},
	imagePath: {
		elementType: 'outlineInput',
		inputType: 'text',
		value: '',
		validationRules: {
			required: true,
		},
		validation: {
			valid: true,
		},
		hidden: true
	},
	foundingDate: {
		elementType: 'outlineInput',
		inputType: 'date',
		icon: 'calendar-alt ',
		value: '',
		label: 'Data założenia',
		validationRules: {
			required: false,
		},
		validation: {
			valid: true
		},
	},
	description: {
		elementType: 'outlineInput',
		inputType: 'textarea',
		icon: 'fas fa-pencil-alt',
		value: '',
		label: 'Opis firmy',
		validationRules: {
			required: false,
		},
		validation: {
			valid: true
		},
	},

}

export default companyInfoFields;