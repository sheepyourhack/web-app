import React, { Component } from 'react';
import { MDBBtn, MDBIcon } from 'mdbreact';
// import { connect } from 'react-redux';
import axios from 'axios';
import deepCopy from 'deepcopy';
import { checkValidity } from '../../helpers/helpers';

import { Redirect } from 'react-router-dom';
import Cookies from 'js-cookie';

import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';

import CompanyInfo from './CompanyInfo/CompanyInfo';
import CompanyAdvert from './CompanyAdvert/CompanyAdvert';
import CompanyContactData from './CompanyContactData/CompanyContactData';
import CompanyNavbar from './CompanyNavbar/CompanyNavbar';
import styles from './MainCompanyPanel.module.scss';
import CompanyEditOffer from './CompanyEditOffer/CompanyEditOffer'
import { style } from 'react-toastify';
import { Switch, Route } from 'react-router-dom';

class MainCompanyPanel extends Component {
	state = {
		// fields: CompanyInfoFields,
		showError: true,
	}
	componentDidMount = async () => {
		const res = await axios.post('/api/auth', { token: Cookies.get("organizationToken") });
		this.props.authCompany(Cookies.get("organizationToken"), res.data.organization)
	}

	render() {
		let location = window.location.href.split("/");
		let link = location[location.length - 1];
		let headerText = '';
		switch (link) {
			case 'info': headerText = 'Informacje';
				break;
			case 'contactData': headerText = 'Dane kontaktowe';
				break;
			case 'advert': headerText = 'Ogłoszenia';
				break;
			case 'edit': headerText = 'Edycja ogłoszeń';
				break;
			case 'interested': headerText = 'Zainteresowani ogłoszeniami';
				break;
		}
		return (
			<div>
				<div className={styles.panel}>
					{console.log(Cookies.get("organizationToken"))}
					{Cookies.get("organizationToken") ? null : <Redirect to="/" />}
					<CompanyNavbar />

					<div className={styles.panelHeader}>
						<h2>{headerText}</h2>
					</div>
					<hr className={styles.border} />
					{/* <CompanyInfo fields={this.state.fields} onChange={this.inputChangedHandler} /> */}

					<Route path="/company" component={CompanyInfo} exact />
					<Route path="/company/info" component={CompanyInfo} exact />
					<Route path="/company/contactData" component={CompanyContactData} exact />
					<Route path="/company/advert" component={CompanyAdvert} exact />
					<Route path="/company/edit" component={CompanyEditOffer} exact />
					<Route path="/company/interested" component={CompanyInfo} exact />

				</div>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		company: state.company.company
	};
};

const mapDispatchToProps = dispatch => {
	return {
		authCompany: (token, company) => dispatch(actionCreators.authCompany(token, company))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(MainCompanyPanel);