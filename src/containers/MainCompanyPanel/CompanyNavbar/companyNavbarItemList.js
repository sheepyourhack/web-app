
const navbarItem = {
    info: {
        name: 'Informacje',
        icon: 'fas fa-home',
        link: 'info',
    },
    contactData: {
        name: 'Dane kontaktowe',
        icon: 'fas fa-phone',
        link: 'contactData',
    },
    advert: {
        name: 'Ogłoszenia',
        icon: 'book-reader',
        link: 'advert',
    },
    edit: {
        name: 'Edycja ogłoszeń',
        icon: 'edit',
        link: 'edit',
    },
    interested: {
        name: 'Zainteresowani ogłoszeniami',
        icon: 'users',
        link: 'interested',
    },

}

export default navbarItem;