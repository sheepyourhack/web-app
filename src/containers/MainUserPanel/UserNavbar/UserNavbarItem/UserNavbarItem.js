import React, { Component } from 'react';
import { MDBIcon } from 'mdbreact';
import { Link } from 'react-router-dom';
import styles from './UserNavbarItem.module.scss'
class UserNavbarItem extends Component {
	render() {
		let location = window.location.href.split("/");
		let link = location[location.length - 1];
		return (
			<Link key={this.props.item.link} to={'/user/' + this.props.item.link} className="link" >
				<div onClick={() => this.props.onClick()} className={styles.itemContainer}>
					<div>
						<MDBIcon style={{ color: this.props.item.link === link ? '#b7156b' : "white" }} size="2x" icon={this.props.item.icon}></MDBIcon>
					</div>
					<div className={styles.name}>{this.props.item.name}</div>
				</div >
			</Link>
		);
	}
}

export default UserNavbarItem;