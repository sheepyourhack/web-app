import React, { Component } from 'react';
import styles from './UserNavbar.module.scss';
import { HamburgerArrow } from 'react-animated-burgers'
import { Motion, spring } from "react-motion";
import UserNavbarItem from './UserNavbarItem/UserNavbarItem'
import navbarItem from './userNavbarItemList.js';

class UserNavbar extends Component {
	state = {
		isActive: false,
		width: 70,
	}

	sidebarButtonClick = () => {
		switch (this.state.isActive) {
			case true:
				this.setState({ isActive: false, width: 70 });
				break;
			case false:
				this.setState({ isActive: true, width: 300 });
				break;
			default:
				this.setState({ isActive: true });
				break;
		}
	}

	navbarItemClick = () => {
		this.setState({ isActive: false, width: 70 });
	}

	render() {
		const navbarElements = [];
		for (const key in navbarItem) {
			navbarElements.push(
				<UserNavbarItem
					key={navbarItem[key].link}
					item={navbarItem[key]}
					onClick={this.navbarItemClick}
				/>
			);
		}

		return (
			<div className={styles.SidebarContainer} >
				<Motion style={{ x: spring(this.state.width, { stiffness: 125 }) }}>
					{({ x }) =>
						<div className={styles.SidebarElement}>
							<div className={styles.button} style={{ width: x + 70 }}>
								<HamburgerArrow className={styles.button} barColor="white" toggleButton={this.sidebarButtonClick} isActive={this.state.isActive} />
							</div>

							<div style={{ width: x }} className={styles.SidebarContent} >
								{/* <UserNavbarItem></UserNavbarItem> */}
								{navbarElements}
							</div>
						</div>
					}
				</Motion>
			</div >
		);
	}
}

export default UserNavbar;