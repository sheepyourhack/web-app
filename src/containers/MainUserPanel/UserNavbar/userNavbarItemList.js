
const navbarItem = {
    info: {
        name: 'Informacje',
        icon: 'fas fa-info-circle',
        link: 'info',
    },
    skills: {
        name: 'Umiejętności',
        icon: 'fas fa-pencil-ruler',
        link: 'skills',
    },
    education: {
        name: 'Wykształcenie',
        icon: 'fas fa-graduation-cap',
        link: 'education',
    },
    offers: {
        name: 'Oferty',
        icon: 'far fa-newspaper',
        link: 'offers',
    },
    experience: {
        name: 'Doświadczenie',
        icon: 'fas fa-hammer',
        link: 'experience',
    },
    hobby: {
        name: 'Hobby',
        icon: 'fas fa-theater-masks',
        link: 'hobby',
    },
}

export default navbarItem;