const userEducationFields = {
	schoolName: {
		elementType: 'outlineInput',
		inputType: 'text',
		icon: 'fas fa-user',
		value: '',
		label: 'Nazwa szkoły/kursu',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
    },
	startDate: {
		elementType: 'outlineInput',
		inputType: 'date',
		icon: 'calendar-alt ',
		value: '',
		label: 'Data rozpoczęcia',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},
	endDate: {
		elementType: 'outlineInput',
		inputType: 'date',
		icon: 'calendar-alt ',
		value: '',
		label: 'Data zakończenia',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},

	description: {
		elementType: 'outlineInput',
		inputType: 'textarea',
		icon: 'fas fa-pencil-alt',
		value: '',
		label: 'Opis szkoły/kursu',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},

}

export default userEducationFields;