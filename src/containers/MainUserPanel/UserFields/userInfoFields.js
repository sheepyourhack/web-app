const userInfoFields = {
	email: {
		elementType: 'outlineInput',
		inputType: 'text',
		icon: 'fas fa-envelope',
		value: '',
		label: 'Email',
		validationRules: {
			required: false,
			isEmail: true,
			error: "Podaj prawidłowy email"
		},
		validation: {
			valid: false,
			isEmail: true
		},
	},
	firstName: {
		elementType: 'outlineInput',
		inputType: 'text',
		icon: 'fas fa-user',
		value: '',
		label: 'Imię',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},
	lastName: {
		elementType: 'outlineInput',
		inputType: 'text',
		icon: 'fas fa-users',
		value: '',
		label: 'Nazwisko',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},
	city: {
		elementType: 'outlineInput',
		inputType: 'text',
		icon: 'fas fa-city',
		value: '',
		label: 'Miasto zamieszkania',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},
	occupation: {
		elementType: 'outlineInput',
		inputType: 'text',
		icon: 'fas fa-user-md',
		value: '',
		label: 'Stanowisko',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},
	occupationPlace: {
		elementType: 'outlineInput',
		inputType: 'text',
		icon: 'fas fa-building',
		value: '',
		label: 'Miejsce pracy/nauki',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},
	imagePath: {
		elementType: 'outlineInput',
		inputType: 'text',
		value: '',
		validationRules: {
			required: true,
		},
		validation: {
			valid: false,
		},
		hidden: true
	},
	birthDate: {
		elementType: 'outlineInput',
		inputType: 'date',
		icon: 'calendar-alt ',
		value: '',
		label: 'Data urodzenia',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},
	description: {
		elementType: 'outlineInput',
		inputType: 'textarea',
		icon: 'fas fa-pencil-alt',
		value: '',
		label: 'Opis siebie',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},
	logoPath: {
		elementType: 'outlineInput',
		inputType: 'text',
		value: '',
		validationRules: {
			required: true,
		},
		validation: {
			valid: true,
		},
		hidden: true
	},
}

export default userInfoFields;