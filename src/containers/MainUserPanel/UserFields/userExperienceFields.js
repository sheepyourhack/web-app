const userExperienceFields = {
	organizationName: {
		elementType: 'outlineInput',
		inputType: 'text',
		icon: 'fas fa-city',
		value: '',
		label: 'Nazwa firmy',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
    },
	startDate: {
		elementType: 'outlineInput',
		inputType: 'date',
		icon: 'calendar-alt ',
		value: '',
		label: 'Data rozpoczęcia',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},
	endDate: {
		elementType: 'outlineInput',
		inputType: 'date',
		icon: 'calendar-alt ',
		value: '',
		label: 'Data zakończenia',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},

	description: {
		elementType: 'outlineInput',
		inputType: 'textarea',
		icon: 'fas fa-pencil-alt',
		value: '',
		label: 'Opis sprawowanego stanowiska',
		validationRules: {
			required: false,
		},
		validation: {
			valid: false
		},
	},

}

export default userExperienceFields;