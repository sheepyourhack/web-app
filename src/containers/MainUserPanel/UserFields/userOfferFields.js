const userOfferFields = {
    title: {
        elementType: 'outlineInput',
        inputType: 'text',
        icon: 'heading',
        value: '',
        label: 'Tytuł ogłosznenia',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    type: {
        elementType: 'select',
        inputType: 'Staż,Praktyka,Wolontariat',
        icon: 'fas fa-pencil-alt',
        value: '',
        label: 'Typ ogłoszenia',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    city: {
        elementType: 'outlineInput',
        inputType: 'text',
        icon: 'city',
        value: '',
        label: 'Miasto',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    paid: {
        elementType: 'select',
        inputType: 'Płatny,Bezpłatny',
        icon: 'coins',
        value: '',
        label: 'Płatny/ Bezpłatny',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    startDate: {
        elementType: 'outlineInput',
        inputType: 'date',
        icon: 'calendar-alt',
        value: '',
        label: 'Data rozpoczęcia',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    endDate: {
        elementType: 'outlineInput',
        inputType: 'date',
        icon: 'calendar-alt',
        value: '',
        label: 'Data zakończenia',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    address: {
        elementType: 'outlineInput',
        inputType: 'text',
        icon: 'map-marker-alt',
        value: '',
        label: 'Adres',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },
    imagePath: {
        elementType: 'outlineInput',
        inputType: 'text',
        value: '',
        validationRules: {
            required: true,
        },
        validation: {
            valid: true,
        },
        hidden: true
    },
    description: {
        elementType: 'outlineInput',
        inputType: 'textarea',
        icon: 'fas fa-pencil-alt',
        value: '',
        label: 'Opis użytkownika',
        validationRules: {
            required: false,
        },
        validation: {
            valid: true
        },
    },


}

export default userOfferFields;