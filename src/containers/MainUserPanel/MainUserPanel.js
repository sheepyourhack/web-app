import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import UserNavbar from './UserNavbar/UserNavbar';
import Cookies from 'js-cookie';
import { Redirect } from 'react-router-dom';
import axios from 'axios';

import styles from './MainUserPanel.module.scss';
import { connect } from 'react-redux';
import * as actionCreators from '../../store/actions/index';

import UserInfo from './UserInfo/UserInfo';
import UserSkills from './UserSkills/UserSkills';
import UserEducation from './UserEducation/UserEducation';
import UserOffers from './UserOffers/UserOffers';
import UserExperience from './UserExperience/UserExperience';
import UserHobby from './UserHobby/UserHobby';

class MainUserPanel extends Component {
    state = {
        // fields: CompanyInfoFields,
        showError: true,
    }

    componentDidMount = async () => {
        const res = await axios.post('/api/auth', { token: Cookies.get("userToken") });
        this.props.authUser(Cookies.get("userToken"), res.data.user)
    }

    render() {
        let location = window.location.href.split("/");
        let link = location[location.length - 1];
        let headerText = '';
        switch (link) {
            case 'info': headerText = 'Informacje';
                break;
            case 'skills': headerText = 'Umiejętności';
                break;
            case 'education': headerText = 'Wykształcenie';
                break;
            case 'offers': headerText = 'Oferty';
                break;
            case 'experience': headerText = 'Doświadczenie';
                break;
            case 'hobby': headerText = 'Hobby';
                break;
            default: headerText = 'Informacje';
                break;
        }
        return (
            <div>
                <div className={styles.panel}>
                    {Cookies.get("userToken") ? null : <Redirect to="/" />}
                    <UserNavbar />

                    <div className={styles.panelHeader}>
                        <h2>{headerText}</h2>
                    </div>
                    <hr className={styles.border} />
                    <div className={styles.content}>
                        <Route path="/user" component={UserInfo} exact />
                        <Route path="/user/info" component={UserInfo} exact />
                        <Route path="/user/skills" component={UserSkills} exact />
                        <Route path="/user/education" component={UserEducation} exact />
                        <Route path="/user/offers" component={UserOffers} exact />
                        <Route path="/user/experience" component={UserExperience} exact />
                        <Route path="/user/hobby" component={UserHobby} exact />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.user.token !== null,
        user: state.user.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        authUser: (token, user) => dispatch(actionCreators.authUser(token, user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainUserPanel);