import React, { Component } from 'react';
import Form from '../../../components/Form/Form';
import styles from './CompanyListItem.module.scss'
import { MDBIcon } from 'mdbreact';

class CompanyDriversItem extends Component {

	state = {
		animation: "fadeIn fast"
	}

	inputChangedHandler = (e, fieldName) => {
		this.props.inputChangedHandler(e, fieldName, this.props.count)
	}

	clickHandler = () => {
		this.props.clickHandler(this.props.count)
	}

	remove = () => {
		this.setState({ animation: "fadeOut faster" })
		setTimeout(() => {
			this.setState({ animation: "" })
			this.props.remove(this.props.count)
		}, 500);
	}

	render() {
		return (
			<div>
					
				<div  className={styles.driversRow + " animated " + this.state.animation}>
					<div className={styles.driversInfo} onClick={this.clickHandler}>
						<Form
							fields={this.props.item}
							inputChangedHandler={this.inputChangedHandler}
							buttonText=""
							button={false}
							showError={this.props.showError}
						/>
					</div>
					<div className={styles.driversRemove}>
						<MDBIcon color="red" className="red-text" icon="times" onClick={this.remove} />
					</div>
				</div>
				{this.props.serverErrorMessage ? <p className={styles.serverErrorMessage}>{this.props.serverErrorMessage}</p> : null}
			</div>
		);
	}
}

export default CompanyDriversItem;