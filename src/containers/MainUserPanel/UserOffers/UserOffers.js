import React, { Component } from 'react';
import Form from '../../../components/Form/Form';
import deepCopy from 'deepcopy';
import { checkValidity } from '../../../helpers/helpers';
import styles from './UserOffers.module.scss';
import userOfferFields from '../UserFields/userOfferFields';
import axios from 'axios';
import { connect } from 'react-redux';
import * as actionCreators from '../../../store/actions/index';
import Cookies from 'js-cookie';

class UserOffers extends Component {
	state = {
		fields: null,
		filled: false,
		requirementsFields: {},
		responsibilitiesFields: {},
		reqNumber: 0,
		resNumber: 0,
	}

	componentDidMount() {
		this.setState({ fields: userOfferFields });
	}

	// fill fields with info only of first redux state change
	static getDerivedStateFromProps(props, state) {
		if (!state.filled && props.company && state.fields) {

			for (const key in state.fields) {
				state.fields[key].value = props.company[key]
				state.fields[key].validation = { valid: true }
			}

			state.filled = true;
		}
		return state;
	}

	isFormValid = () => {
		for (const field in this.state.fields) {
			if (!this.state.fields[field].validation.valid) return false;
		}

		return true;
	}

	inputChangedHandler = async (e, fieldName) => {
		const fields = deepCopy(this.state.fields);
		fields[fieldName].value = e.target.value;
		fields[fieldName].touched = true;
		fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);
		this.setState({ fields: fields });
	}

	inputChangedHandlerReq = (e, fieldName) => {
		const fields = deepCopy(this.state.requirementsFields);
		fields[fieldName].value = e.target.value;
		fields[fieldName].touched = true;
		fields[fieldName].add = true;
		fields[fieldName].validation = checkValidity(e.target.value, fields[fieldName].validationRules);
		console.log(fields[fieldName].add)
		this.setState({ requirementsFields: fields });
	}

	sendData = async () => {
		this.setState({ showError: true });
		if (this.isFormValid()) {
			const updateObject = {}
			for (const key in this.state.fields) {
				updateObject[key] = this.state.fields[key].value
			}

			updateObject.ownerName = this.props.user.firstName + " " + this.props.user.lastName;
			updateObject.ownerId = this.props.user._id;
			updateObject.ownerType = "Użytkownik";
			updateObject.paid = updateObject.paid === "Płatny" ? true : false;

			const res = await axios.post(`/api/offer/add`, updateObject);
			console.log(res);
		}
	}

	sendFile = async (image) => {
		if (image) {
			const formData = new FormData();
			formData.append('image', image, image.name);

			const res = await axios({
				url: '/api/image/add',
				method: 'POST',
				headers: { 'Authorization': `Bearer ${this.props.token}`, 'Content-Type': 'multipart/form-data' },
				data: formData
			});

			if (!res.data.error) {
				const fields = this.state.fields;
				fields.imagePath.value = res.data.imagePath;
				fields.imagePath.valid = { valid: true }
				this.setState({ fields });
			}
		}
	}

	changeSelect = (e, fieldName) => {
		const fields = deepCopy(this.state.fields);
		fields[fieldName].value = e.target.value;
		this.setState({ fields: fields });
	}
	render() {
		return (
			<div className={styles.info}>
				{this.state.fields ?
					<>
						<div className={styles.mainDiv}>
							<div className={styles.infoDiv}>
								<Form
									fields={this.state.fields}
									inputChangedHandler={this.inputChangedHandler}
									showError={this.state.showError}
									clickHandler={this.sendData}
									buttonText="Zapisz dane"
									button={true}
									changeSelect={this.changeSelect}
								/>
							</div>
							<div className={styles.imgDiv}>
								{this.state.fields.imagePath.value ? <img src={`/api/image/${this.state.fields.imagePath.value}`} className={styles.logo} alt="logo" /> : null}

								<label className={styles.label} htmlFor="upload-photo">Wybierz zdjęcie...</label>
								<input className={styles.inputFile} id="upload-photo" type="file" onChange={(e) => this.sendFile(e.target.files[0])} />
							</div>
						</div>
					</> : null}
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		user: state.user.user
	};
};

const mapDispatchToProps = dispatch => {
	return {
		authUser: (token, user) => dispatch(actionCreators.authUser(token, user))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(UserOffers);