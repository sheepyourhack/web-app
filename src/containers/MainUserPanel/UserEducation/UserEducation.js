import React, { Component } from 'react';
import CompanyListItem from '../CompanyListItem/CompanyListItem'
import userEducationFields from '../UserFields/userEducationFields'
import deepcopy from 'deepcopy';
import { checkValidity } from '../../../helpers/helpers';
import styles from './UserEducation.module.scss'
import { MDBBtn } from 'mdbreact';
import axios from 'axios';
import { connect } from 'react-redux';
import * as actionCreators from '../../../store/actions/index';
import Cookies from 'js-cookie';

class UserEducation extends Component {

	state = {
		fields: [],
	}

	// fill fields with info only of first redux state change
	static getDerivedStateFromProps(props, state) {
		const fields = [];
		if (!state.filled && props.user && state.fields) {

			props.user.education.map(education => {

				const form = deepcopy(userEducationFields);
				for (const key in form) {
					if (key === "startDate" || key === "endDate") {
						form[key].value = new Date(education[key]).toISOString().slice(0, 10);
					} else
						form[key].value = education[key]
				}
				fields.push({ showError: false, serverErrorMessage: null, fields: form });
				state.fields = fields;
			});

			state.filled = true;
		}
		fields.push({ showError: false, serverErrorMessage: null, fields: deepcopy(userEducationFields) });
		return state;
	}

	inputChangedHandler = async (e, fieldName, count) => {
		const fields = deepcopy(this.state.fields);
		fields[count].fields[fieldName].value = e.target.value;
		fields[count].fields[fieldName].touched = true;
		fields[count].fields[fieldName].validation = checkValidity(e.target.value, fields[count].fields[fieldName].validationRules);
		this.setState({ fields: fields });
	}

	clickHandler = async (count) => {
		if (count === this.state.fields.length - 1) {
			const fields = deepcopy(this.state.fields);
			fields.push({ showError: false, serverErrorMessage: null, fields: userEducationFields })
			await this.setState({ fields: fields });
		}
	}

	removeDriver = async (count) => {
		if (this.state.fields.length > 1) {
			const fields = deepcopy(this.state.fields);
			fields.splice(count, 1)
			await this.setState({ fields: fields });
		}
	}

	isFormValid = (fields) => {
		for (const field in fields) {
			if (!fields[field].validation.valid) return false;
		}

		return true;
	}

	sendData = async () => {
		const arr = [];

		for (const field in this.state.fields) {
			const obj = {}
			for (const key in this.state.fields[field].fields) {
				obj[key] = this.state.fields[field].fields[key].value;
			}
			arr.push(obj);
		}
		arr.pop();

		const res = await axios.patch(`/api/user/${this.props.user._id}`, { education: arr });

		this.props.authUser(Cookies.get("userToken"), res.data.user);
	}



	render() {
		return (
			<div>
				{this.state.fields.map((item, i) =>
					<CompanyListItem
						key={i}
						showError={item.showError}
						serverErrorMessage={item.serverErrorMessage}
						count={i}
						item={item.fields}
						inputChangedHandler={this.inputChangedHandler}
						clickHandler={this.clickHandler}
						remove={this.removeDriver} />
				)}
				<div className={styles.buttonDiv}>
					<span className="text-light">Klikaj na kolejne wiersze aby dodać nowy kurs/szkołę.</span>
					<MDBBtn className="font-weight-bold" color="unique" size="sm" onClick={this.sendData}>Dodaj wykształcenie</MDBBtn>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.user.token !== null,
		user: state.user.user
	};
};

const mapDispatchToProps = dispatch => {
	return {
		authUser: (token, user) => dispatch(actionCreators.authUser(token, user))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(UserEducation);