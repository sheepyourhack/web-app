import React, { Component } from 'react';
import styles from './UserSkills.module.scss';
import { MDBInput, MDBBtn } from 'mdbreact';
import deepCopy from 'deepcopy';
import axios from 'axios';
import { connect } from 'react-redux';
import * as actionCreators from '../../../store/actions/index';
import Cookies from 'js-cookie';

class UserSkills extends Component {
    state = {
        skillsCount: 1,
        skillsArr: [""]
    }

    // fill fields with info only of first redux state change
    static getDerivedStateFromProps(props, state) {
        if (!state.filled && props.user) {
            state.skillsArr = props.user.skills;
            state.skillsCount = props.user.skills.length + 1;

            state.filled = true;
        }
        return state;
    }

    click = (e) => {
        if ((e.currentTarget.getAttribute("data-count") == this.state.skillsCount - 1) &&
            ((parseInt(e.currentTarget.getAttribute("data-count")) - 1 < 0) ||
                (this.state.skillsArr[parseInt(e.currentTarget.getAttribute("data-count")) - 1] != "")
            )


        ) {
            let fields = deepCopy(this.state.skillsArr);
            fields.push("")

            this.setState((prevState) => {
                return { skillsCount: prevState.skillsCount + 1, skillsArr: fields }
            })
        }
    }

    change = async (e) => {
        const fields = deepCopy(this.state.skillsArr);
        fields[parseInt(e.currentTarget.getAttribute("data-count"))] = e.currentTarget.value;
        await this.setState({ skillsArr: fields })
    }

    sendData = async () => {
        const skills = this.state.skillsArr.filter(x => x !== "");
        const res = await axios.patch(`/api/user/${this.props.user._id}`, { skills: skills });

        this.props.authUser(Cookies.get("userToken"), res.data.user);
    }

    render() {
        let skills = []
        for (let i = 0; i < this.state.skillsCount; i++) {
            skills.push(<div key={i} style={{ margin: "10px" }}><MDBInput value={this.state.skillsArr[i]} label="Wpisz następna umiejętność" onFocus={this.click} onChange={this.change} data-count={i} outline className={styles.skillInput} /></div>)
        }

        return (
            <div className={styles.main} onClick={this.test}>
                <div className={styles.inputList}>
                    {skills}
                </div>
                <div className={styles.buttonDiv}>
                    <span className="text-light">Klikaj na kolejne wiersze aby dodać nowy kurs/szkołę.</span>
                    <MDBBtn className="font-weight-bold" color="unique" size="sm" onClick={this.sendData}>Dodaj wykształcenie</MDBBtn>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.user.token !== null,
        user: state.user.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        authUser: (token, user) => dispatch(actionCreators.authUser(token, user))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserSkills);