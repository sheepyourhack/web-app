import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Navbar from './containers/Navbar/Navbar';

import styles from './App.module.scss';

import MainCompanyPanel from './containers/MainCompanyPanel/MainCompanyPanel';
import MainUserPanel from './containers/MainUserPanel/MainUserPanel';
import MainPage from './containers/MainPage/MainPage';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <div className={styles.container}>
          <Switch>
            <Route path="/" component={MainPage} exact />
            <Route path="/company" component={MainCompanyPanel} />
            <Route path="/user" component={MainUserPanel} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
