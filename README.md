# WorkHUB
Simple application to search work, practice and voloonteer created on Hackhaton '**SheepYourHack** in 2019' in **24 hours**.

## Table of Contents
 - [General info](#general-info) 
 - [Technologies](#technologies) 
 - [Setup](#setup) 
 
## General info 
### Normal Users
The application allows to **register normal users** and change they basic information, work experience, skills, hobbies and allows to add new offer to search work. 

| *Offers*  | *Selected offers*  | *Register form*  |
| -- | -- | -- |
| ![Register](https://i.ibb.co/R0wp6GJ/workhub.png "Main offers") | ![Register](https://i.ibb.co/XyKry29/workhub1.png "Selected offers") | ![Register](https://i.ibb.co/yqFddWd/workhub2.png "Register form") |

| *Basic information screen* | *Skills screen* | *Add offer screen* |
| -- | -- | -- |
|  ![Register](https://i.ibb.co/z4zr75d/workhub3.png "Basic information") | ![Register](https://i.ibb.co/687F8pC/workhub4.png "Skills") | ![Register](https://i.ibb.co/ZM2j4Dx/workhub5.png "Add offer") |

### Companies
Additionally application allows to **register companies** and search new employers, add offers and set basic information about the business.

| *Basic information*  | *Contact detail*  | *Offers*  |
| -- | -- | -- |
| ![Register](https://i.ibb.co/vPm0rqJ/workhub.png "Basic info") | ![Register](https://i.ibb.co/b583jmx/workhub1.png "contact details") | ![Register](https://i.ibb.co/0Z9fMqc/workhub3.png "add offer") |


## Technologies
 - Node.js 12.2.0 (server API)
 - React 16.8.4 
 - Redux 4.0.1
 
## Setup 
To run server: install ``` npm install``` and run ``` npm start ``` 
  
To run frontend: install``` npm install ``` and run``` npm start ``` 